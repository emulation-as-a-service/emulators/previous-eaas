from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="previous"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"


RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
g++ cmake subversion zlib1g-dev libpng-dev libsdl2-dev

RUN svn checkout svn://svn.code.sf.net/p/previous/code/branches/branch_realtime previous-code

workdir  previous-code
workdir src
add add-options.patch .
run svn patch add-options.patch
workdir ..
run ./configure
run make install
